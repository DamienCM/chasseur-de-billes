#pragma once

#include <iostream>

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>

using namespace cv;
using namespace std;

int load_image(Mat &image, const string IMAGE_PATH, const bool DISPLAY = true, const bool PRINT = true);

int undistort_image(Mat &image, Mat &undistorded, const string CAMERA_CORR_PATH, const bool DISPLAY = true, const bool PRINT = true);

int crop_image(Mat &image, Mat &cropped, const Point2i UPPER_LEFT, const Point2i LOWER_RIGHT, const bool DISPLAY = true, const bool PRINT = true);

Scalar ScalarRGB2HSV(uchar R, uchar G, uchar B);

int hsv_threshold(Mat &image, Mat &thresholded_image, const Scalar COLOR, const int VAR_H, const int VAR_S, const int VAR_V, const bool DISPLAY = true, const bool PRINT = true);

int filter_image(Mat &image, Mat &filtered, const int KERNEL_SIZE, const int ITERATIONS, const bool DISPLAY = true, const bool PRINT = true, const int TYPE = MORPH_CLOSE);

int detect_circles(vector<Vec3f> &centers, Mat &image_in, Mat &image_out, const int MIN_RADIUS, const int MAX_RADIUS, const int MAX_NUMBER, const double PARAM1, const double PARAM2, const bool DISPLAY = true, const bool PRINT = true);

int find_arrival(Point2f &arrival, const Point2f A, const Point2f B, const Point2f C, const Point2f D);
