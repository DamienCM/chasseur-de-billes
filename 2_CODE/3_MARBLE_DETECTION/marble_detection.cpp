//todo appliquer filtres puis faires detection de contours puis estimer les coordonnées au centre. Detecter les contours des barrres pour ensuite predire les rebonds

#include "marble_detection_utils.hpp"

using namespace cv;
using namespace std;

void print_menu()
{
    cout << endl;
    cout << "0. Quit" << endl;
    cout << "1. Set path" << endl;
    cout << "2. Load image -- undistord it -- crop it" << endl;
    cout << "3. Threshold" << endl;
    cout << "4. Filters" << endl;
    cout << "5. Detect circles" << endl;
}

// --- MAIN ---

int main()
{
    // ------- INITIALIZATION -------

    // Variables
    Mat image;          // input image from the file
    Mat undistorded;    // undistorded image
    Mat cropped;        // cropped image
    string path;        // path to the image file
    string camera_path; // path to the camera calibration file
    string display_str; // string to select display mode
    bool display;       // boolean to select display mode
    string print_str;   // string to select print mode
    bool print;         // boolean to select print mode
    string choice;      // string to select the subprogram to be launched

    // Cropping parameters
    const Point2i CROP_UP_LEFT(400, 10);
    const Point2i CROP_DOWN_RIGHT(1500, 870);

    // Thresholds parameters
    // colors
    //todo trouver les bonnes couleurs
    const Scalar MARBLE_COLOR = Scalar(21, 63, 149);       //BGR  Balle bleue
    const Scalar BACKGROUND_COLOR = Scalar(182, 160, 119); //BGR
    const Scalar MARBLE_COLOR_2 = Scalar(30, 30, 41);      //BGR
    const Scalar BARS_COLOR = Scalar(130, 33, 56);         //BGR
    // Variance HSV
    //todo trouver les bonnes valeurs
    const int VAR_H = 10;
    const int VAR_S = 40;
    const int VAR_V = 40;

    // Masks Matrices to be computed
    Mat mask_marble;     // mask for marble
    Mat mask_marble_2;   // mask for marble 2
    Mat mask_background; // mask for background
    Mat mask_bars;       // mask for bars

    // Filters parameters
    Mat filtered_image;         // filtered image
    const int KERNEL_SIZE = 20; // kernel size for the closing kernel
    const int ITERATIONS = 1;   // number of iterations for the closing kernel to be operated

    // Circles parameters
    Mat circle_detection_image;       // image to be used for circle detection
    vector<Vec3f> marbles_centers;    // centers of circles
    const int MARBLE_NUMBER = 1;      // max number of circles to be detected (ie nombre max de bille simultanee)
    const int MARBLE_MIN_RADIUS = 30; // radius of circles
    const int MARBLE_MAX_RADIUS = 70; // radius of circles
    const double PARAM1 = 300;        // peu d'importance
    const double PARAM2 = 12;         // 4..16    4 <-- bcp de cercle detecte ; 16 <-- peu de cercle detecte

    // Scalar for arrival
    Point2f A = Point2f(2, 2);
    Point2f B = Point2f(5, 8);
    Point2f C = Point2f(-1, 1);
    Point2f D = Point2f(2, 0);

    Point2f M;

    // --------------------------------

    // ------- DISPLAY and PRINT : ON / OFF -------
    cout << "Display ? 0 : off, 1 : on, default = on" << endl;
    getline(cin, display_str);
    // convert to boolean
    display = (display_str == "on") || (display_str == "1") || (display_str == "") || (display_str == "true");
    if (display)
        cout << "Display on" << endl;
    else
        cout << "Display off" << endl;

    // same for printing
    cout << "Print ? 0 : off, 1 : on, default = on" << endl;
    getline(cin, print_str);
    // convert to boolean
    print = (print_str == "on") || (print_str == "1") || (print_str == "") || (print_str == "true");
    if (print)
        cout << "Print on" << endl;
    else
        cout << "Print off" << endl;

    // --------------------------------

    // ----- MAIN LOOP ----------------
    while (choice != "0")
    {
        print_menu();
        getline(cin, choice);
        cout << "Lauching choice : " << choice << endl;

        if (choice == "0") // Quit
        {
            cout << "Quitting" << endl;
            return 0;
        }

        else if (choice == "1") // Set path
        {
            cout << "Enter image path: " << endl;
            getline(cin, path);
            if (path == "")
                path = "./i.jpg";
            cout << "Image path set to: " << path << endl;

            cout << "Enter camera calibration path: " << endl;
            getline(cin, camera_path);
            if (camera_path == "")
                camera_path = "./camera_calibration.json";
            cout << "Camera calibration path set to: " << camera_path << endl;
        }
        else if (choice == "2") // Load image
        {
            load_image(image, path, display, print);
            undistort_image(image, undistorded, camera_path, display, print);
            crop_image(undistorded, cropped, CROP_UP_LEFT, CROP_DOWN_RIGHT, display, print);
        }

        else if (choice == "3") // Thresholds
        {
            hsv_threshold(image, mask_marble, MARBLE_COLOR, VAR_H, VAR_S, VAR_V, display, print);
            // hsv_threshold(image, mask_background, BACKGROUND_COLOR, VAR_H, VAR_S, VAR_V, display);
            // hsv_threshold(image, mask_marble_2, MARBLE_COLOR_2, VAR_H, VAR_S, VAR_V, display);
            // hsv_threshold(image, mask_bars, BARS_COLOR, VAR_H, VAR_S, VAR_V, display);
        }

        else if (choice == "4") // Filters
        {
            filter_image(mask_marble, filtered_image, KERNEL_SIZE, ITERATIONS, display, print);
        }

        else if (choice == "5") // Detect circles
        {
            detect_circles(marbles_centers, filtered_image, circle_detection_image, MARBLE_MIN_RADIUS, MARBLE_MAX_RADIUS, MARBLE_NUMBER, PARAM1, PARAM2, display, print);
        }

        else if (choice == "6")
        {
            find_arrival(M, A, B, C, D);
            cout << "M = " << endl;
            cout << M << endl;
        }

        else // wrong input loop back
        {
            cout << "Invalid choice" << endl;
        }
    }
    // --------------------------------
}