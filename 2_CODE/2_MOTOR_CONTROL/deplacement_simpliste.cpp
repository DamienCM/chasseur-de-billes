
#include <iostream>
#include <wiringPi.h>
#include <softPwm.h>

using namespace std;

// #define en_pin 21
// #define in1_pin 20
// #define in2_pin 16

const int en_pin = 1;
const int in1_pin = 5;
const int in2_pin = 4;

int main(void)
{
    if ( wiringPiSetupGpio() == -1 ){
        cout << "setup wiring de merde";
        return 1;
        }
    // setup pour le package wiringPi qui possede les fonctions pwmWrite et digitalWrite
    wiringPiSetup();
    
    // parametrisation des pins
    // une pour la pwm (equivalent vistesse)
    pinMode(en_pin, PWM_OUTPUT);
    // deux pour le pont en H (sens de rotation)
    pinMode(in1_pin, OUTPUT);
    pinMode(in2_pin, OUTPUT);

    // valeurs par default
    digitalWrite(in1_pin, LOW);
    digitalWrite(in2_pin, LOW);
    pwmWrite(en_pin, 0);

    int v = 1024 ;
    pwmWrite(en_pin, v);

    digitalWrite(in1_pin, HIGH);
    digitalWrite(in2_pin, LOW);
    cout << "droite ";
    delay(1000);
    digitalWrite(in1_pin, LOW);
    digitalWrite(in2_pin, HIGH);
    cout << "gauche ";
    delay(1000);
    digitalWrite(in1_pin, LOW);
    digitalWrite(in2_pin, LOW);
    cout << "stop ";

    return 0;
}