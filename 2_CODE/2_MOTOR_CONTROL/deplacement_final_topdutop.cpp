// test pwm
#pragma once

#include <iostream>
#include <wiringPi.h>
#include <softPwm.h>

using namespace std;

// ATTRIBUTION DES PINS

const int en_pin = 1;
const int in1_pin = 5;
const int in2_pin = 4;

// FONCTIONS POUR GERER LES PARAMETRES DU PONT EN H

// rotation du moteur dans un sens
void gauche()
{
    digitalWrite(in1_pin, HIGH);
    digitalWrite(in2_pin, LOW);
}
// rotation dans l'autre sens
void droite()
{
    digitalWrite(in1_pin, LOW);
    digitalWrite(in2_pin, HIGH);
}
// arret du moteur
void stop()
{
    digitalWrite(in1_pin, LOW);
    digitalWrite(in2_pin, LOW);
}

// reglage de la vitesse sur une echelle de 0 a 1024
void vitesse(int vitesse)
{
    if (vitesse>=0 && vitesse<1024)
    {
        pwmWrite(en_pin, vitesse);
    }
    else if (vitesse>=1024)
    {
        pwmWrite(en_pin,1024);
    }
    else
    {
        pwmWrite(en_pin,0);
    }
}

// FONCTIONS POUR LE DEPLACEMENT

int get_pos_actuelle()
{
    int pos_actuelle ;
    pos_actuelle = 1000 ; // normalement a determiner par la vision
    return pos_actuelle ;
}

int get_pos_desiree()
{
    int pos_desiree ;
    pos_desiree = 500 ; // normalement determinee par la prevision de trajectoire
    return pos_desiree ;
}

// FONCTION POUR INITIALISER LE MOTEUR

void fonc()
{
    wiringPiSetup();
    // parametrisation des pins
    pinMode(en_pin, PWM_OUTPUT);
    pinMode(in1_pin, OUTPUT);
    pinMode(in2_pin, OUTPUT);
    // initialisation du sens de rotation et de la vitesse
    stop();
    vitesse(0);
}

// fonction grossiere avant de faire un PID
void bouger_position()
{
    int k = 0 ;
    int nb_iter_max = 20 ; // pour quitter la boucle while quand on atteint un nb max d'iterations
    int pas = 20 ; // erreur toleree entre la consigne et la position de la boite
    int tps = 100 ; // temps durant lequel les moteurs sont actives
    int position = get_pos_actuelle() ;
    int consigne = get_pos_desiree() ;
    int erreur = position - consigne ;

    // boucle qui actionne le moteur dans le sens desire tant que 
    // la cage n'est pas dans la bonne position, a une erreur pres appelee "pas"
    while(abs(erreur)>=pas && k<nb_iter_max){
        if (erreur > 0)
        {
            droite();
            delay(tps);
            stop();
            k = k+1 ;
            // valeurs factices pour voir si la fonction fonctionne
            position = position - 100 ;
            // position = get_pos_actuelle() ;
            erreur = position - consigne ;
        }
        else if (erreur <0)
        {
            gauche();
            delay(tps);
            stop();
            k = k+1 ;
            // valeurs factices pour voir si la fonction fonctionne
            position = position + 100 ;
            // position = get_pos_actuelle() ;
            erreur = position - consigne ;
        }
        else
        {
            stop();
            cout << "euh je ne sais pas ";
        }
    }
}

// MAIN

// int main(void)
// {
//     //    if ( wiringPiSetupGpio() == -1 ){
//     //        cout << "setup wiring de merde ";
//     //        return 1;
//     //    }

//     fonc();
//     cout << "main ok" << endl;
//     droite();

//     // softPwmCreate(en_pin,0,100);

//     for(int i=0; i<=1;i=i+1)
//     {
//         vitesse(1024);
//         delay(2000);
//         cout << "max" << endl;
//         vitesse(512);
//         delay(2000);
//         cout << "demi" << endl;        
//         vitesse(0);
//         delay(2000);
//         cout << "nulle" << endl;
//         gauche();
//     }
//     stop();
//     return 0;
// }
