// test pwm

#include <iostream>
#include <wiringPi.h>
#include <softPwm.h>

using namespace std;


const int en_pin = 1;
const int in1_pin = 5;
const int in2_pin = 4;


void foncInit()
{
     // parametrisation des pins
     // une pour la pwm (equivalent vistesse)
     pinMode(en_pin, PWM_OUTPUT);
     // deux pour le pont en H (sens de rotation)
     pinMode(in1_pin, OUTPUT);
     pinMode(in2_pin, OUTPUT);

     // valeurs par default
     digitalWrite(in1_pin, LOW);
     digitalWrite(in2_pin, LOW);
     pwmWrite(en_pin, 0);
}       


void vitesse(int vitesse)
{
    // transmet 
    if (vitesse>=0 && vitesse<1024)
    {
        pwmWrite(en_pin, vitesse);
    }

}

// FONCTIONS POUR LES PARAMETRES DU PONT EN H

void stop()
{
    // regle les pins du pont en H pour erreter les moteurs
    digitalWrite(in1_pin, LOW);
    digitalWrite(in2_pin, LOW);
    cout << "stop ";
}

void droite()
{
    // regle les pins du pont en H pour aller dans le sens 'droite'
    digitalWrite(in1_pin, HIGH);
    digitalWrite(in2_pin, LOW);
    cout << "droite ";
}

void gauche()
{
    // regle les pins du pont en H pour aller dans le sens 'gauche'
    digitalWrite(in1_pin, LOW);
    digitalWrite(in2_pin, HIGH);
    cout << "gauche ";
}

// FONCTIONS POUR LE DEPLACEMENT

int get_pos_actuelle()
{
    int pos_actuelle ;
    pos_actuelle = 1000 ; // normalement a determiner par la vision
    return pos_actuelle ;
}

int get_pos_desiree()
{
    int pos_desiree ;
    pos_desiree = 500 ; // normalement determinee par la prevision de trajectoire
    return pos_desiree ;
}

// fonction grossiere avant de faire un PID
void bouger_position()
{
    int k = 0 ;
    int nb_iter_max = 20 ; // pour quitter la boucle while quand on atteint un nb max d'iterations
    int pas = 20 ; // erreur toleree entre la consigne et la position de la boite
    int tps = 100 ; // temps durant lequel les moteurs sont actives
    int position = get_pos_actuelle() ;
    int consigne = get_pos_desiree() ;
    int erreur = position - consigne ;

    // boucle qui actionne le moteur dans le sens desire tant que 
    // la cage n'est pas dans la bonne position, a une erreur pres appelee "pas"
    while(abs(erreur)>=pas && k<nb_iter_max){
        if (erreur > 0)
        {
            droite();
            delay(tps);
            stop();
            k = k+1 ;
            // valeurs factices pour voir si la fonction fonctionne
            position = position - 100 ;
            // position = get_pos_actuelle() ;
            erreur = position - consigne ;
        }
        else if (erreur <0)
        {
            gauche();
            delay(tps);
            stop();
            k = k+1 ;
            // valeurs factices pour voir si la fonction fonctionne
            position = position + 100 ;
            // position = get_pos_actuelle() ;
            erreur = position - consigne ;
        }
        else
        {
            stop();
            cout << "euh je ne sais pas ";
        }
    }
}

// <<<<<<<<<<<<<<<<<<<<<<<<<<<< PID >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>





int main(void)
{
    //    if ( wiringPiSetupGpio() == -1 ){
    //        cout << "setup wiring de merde ";
    //        return 1;
    //    }
    // setup pour le package wiringPi qui possede les fonctions pwmWrite et digitalWrite
    wiringPiSetup();
    
    foncInit();
    int v = 512 ;
    vitesse(v);
    droite();
    delay(1000);
    gauche();
    delay(1000);
    stop();
    return 0;
}