# Motor Control 

## Objectifs 
1. Obtenir une image de la camera
2. Contrôler le moteur de façon analogique
3. Asservir le moteur 
4. Commande prédictive

## 1. Obtenir un image de la camera
Paramètres et fonctions appelées

## 2. Contrôler le moteur de façon analogique
Fonctions :
    - pwm()
    - io()

## 3.  Asservir le moteur
Fonctions :
    - get_current_pos()
        permet d'obtenir la position actuelle du moteur avec la tag ArUco
    - position()
        Donne une commande de position
    - vitesse()
        Fixe une vitesse


## 4. Commande prédictive
Fonctions :
    - position_arrivee_prevue(Pt, Pt-1)
    - eviter()
    - attraper()