#include "opencv2/aruco.hpp"
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>
#include <vector>
using namespace cv;
using namespace std;
int main()
{
    Mat input_image;
    input_image = imread("marker.jpg");
    // Display the image
    imshow("input_image", input_image);
    waitKey(0);
    // Identify ArUco markers
    vector<int> markerIds;
    vector<vector<Point2f>> markerCorners, rejectedCandidates;
    Ptr<aruco::DetectorParameters> parameters = aruco::DetectorParameters::create();
    Ptr<aruco::Dictionary> dictionary = aruco::getPredefinedDictionary(aruco::DICT_6X6_250);
    cv::aruco::detectMarkers(input_image, dictionary, markerCorners, markerIds, parameters, rejectedCandidates);
    // Display the image with the markers
    cout << "Total markers detected " << markerIds.size() << endl;
    Mat outputImage = input_image.clone();
    aruco::drawDetectedMarkers(outputImage, markerCorners, markerIds);
    imshow("outputImage", outputImage);
    waitKey(0);


    return 0;
}
