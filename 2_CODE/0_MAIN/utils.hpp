#pragma once

#include <iostream>

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/aruco.hpp>
#include <string>


#include <wiringPi.h>

// ATTRIBUTION DES PINS

const int EN_PIN = 1;
const int IN1_PIN = 5;
const int IN2_PIN = 4;

using namespace cv;
using namespace std;

int load_image(Mat &image, const string IMAGE_PATH, const bool DISPLAY, const bool PRINT);

int undistort_image(Mat &image, Mat &undistorded, Mat &cameraMatrix, Mat &distCoeffs, const bool DISPLAY, const bool PRINT);

int crop_image(Mat &image, Mat &cropped, const Point2i UPPER_LEFT, const Point2i LOWER_RIGHT, const bool DISPLAY, const bool PRINT);

Scalar ScalarRGB2HSV(uchar R, uchar G, uchar B);

int hsv_threshold(Mat &image, Mat &thresholded_image, const Scalar COLOR, const int VAR_H, const int VAR_S, const int VAR_V, const bool DISPLAY, const bool PRINT);

int filter_image(Mat &image, Mat &filtered, const int KERNEL_SIZE, const int ITERATIONS, const bool DISPLAY, const bool PRINT, const int TYPE);

int detect_circles(vector<Vec3f> &centers, Mat &image_in, Mat &image_out, const int MIN_RADIUS, const int MAX_RADIUS, const int MAX_NUMBER, const double PARAM1, const double PARAM2, const bool DISPLAY, const bool PRINT);

int find_arrival(Point2f &arrival, const Point2f A, const Point2f B, const Point2f C, const Point2f D);

int load_camera_params(string filename, Mat &cam_matrix, Mat &dist_coeffs);

int detect_markers(Mat &frame, Mat &cam_matrix, const float &ARUCO_SIZE, Mat &dist_coeffs, Ptr<aruco::Dictionary> &dictionary, Ptr<aruco::DetectorParameters> parameters, vector<vector<Point2f>> &corners, vector<Vec3d> &rvecs, vector<Vec3d> &tvecs, vector<int> &ids, vector<vector<Point2f>> rejectedCandidates);

void gauche();

void droite();

void stop();

void vitesse(int vitesse);

int get_pos_actuelle();

int get_pos_desiree();

void init_wirinPi();

void bouger_position();

float compute_distance(float &dist, Mat &im, Point2f Ma, Point2f Mb, const Point2f A, const Point2f B);


