#include "utils.hpp"

int detect_markers(Mat &frame, Mat &cam_matrix, const float &ARUCO_SIZE, Mat &dist_coeffs, Ptr<aruco::Dictionary> &dictionary, Ptr<aruco::DetectorParameters> parameters, vector<vector<Point2f>> &corners, vector<Vec3d> &rvecs, vector<Vec3d> &tvecs, vector<int> &ids, vector<vector<Point2f>> rejectedCandidates)
{
    aruco::detectMarkers(frame, dictionary, corners, ids, parameters, rejectedCandidates);

    // if (ids.size() > 0)
    // {
    // Point2f cent(0, 0);
    //     for (int p = 0; p < 4; p++)
    //         cent += currentMarker.ptr<Point2f>(0)[p];
    //     cent = cent / 4.;
    //     // aruco::estimatePoseSingleMarkers(corners, ARUCO_SIZE, cam_matrix, dist_coeffs, rvecs, tvecs);
    // }
    return 0;
}

int load_camera_params(string filename, Mat &cam_matrix, Mat &dist_coeffs)
{
    FileStorage fs(filename, FileStorage::READ);
    if (!fs.isOpened())
    {
        cout << "Failed to open file " << filename << endl;
        return -1;
    }
    fs["cameraMatrix_1280x720"] >> cam_matrix;
    fs["distCoeffs_1280x720"] >> dist_coeffs;
    // cout << "Camera matrix: " << cam_matrix << endl;
    // cout << "Distortion coefficients: " << dist_coeffs << endl;
    return 0;
}

#include <iostream>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d.hpp>

using namespace cv;
using namespace std;

int load_image(Mat &image, const string IMAGE_PATH, const bool DISPLAY = true, const bool PRINT = true)
{
    /**
     * Load the image to the Mat image from the file and apply the correction from the camera matrix and coefficients.
     * 
     * @param  {Mat} image                : in-out matrice to save the image
     * @param  {string} IMAGE_PATH        : path to the image to be loaded
     * @param  {bool} DISPLAY             : choose to display or not the loaded image
     * @param  {bool} PRINT               : choose to print or not informations during the process
     * @return {int}                      : error code if error -1, else 0
     */

    // load the image
    if (PRINT)
        cout << "Loading image ..." << IMAGE_PATH << endl;
    image = imread(IMAGE_PATH, IMREAD_COLOR);

    // check if the image is loaded
    if (image.empty())
    {
        cout << "Could not open or find the image!\n"
             << endl;
        return -1;
    }
    if (PRINT)
        cout << "Image loaded" << endl;

    // display the image
    if (DISPLAY)
    {
        namedWindow("Display Image", WINDOW_NORMAL);
        resizeWindow("Display Image", 1920, 1080);
        imshow("Display Image", image);
        waitKey(0);
        destroyAllWindows();
    }
    return 0;
}

int undistort_image(Mat &image, Mat &undistorded, Mat &cameraMatrix, Mat &distCoeffs, const bool DISPLAY = true, const bool PRINT = true)
{
    /**
     * Undistort the image.
     * 
     * @param  {Mat} image                : ub matrice to save the image
     * @param  {Mat} undistorded          : out matrice to save the undistorded image
     * @param  {string} CAMERA_CORR_PATH  : path to the camera correction file
     * @param  {bool} DISPLAY             : choose to display or not the loaded image
     * @param  {bool} PRINT               : choose to print or not informations during the process
     * @return {int}                      : error code if error -1, else 0
     */

    // load the camera matrix and distortion coefficients
    if (PRINT)
        cout << "Loading camera matrix and distortion coefficients ..." << endl;

    // FileStorage fs(CAMERA_CORR_PATH, FileStorage::READ);
    // if (!fs.isOpened())
    // {
    //     cout << "Could not open camera correction file!\n"
    //          << endl;
    //     return -1;
    // }
    // Mat cameraMatrix, distCoeffs;
    // fs["cameraMatrix"] >> cameraMatrix;
    // fs["distCoeffs"] >> distCoeffs;
    // fs.release();

    // undisort the image
    if (PRINT)
    {
        cout << "Camera matrix and distortion coefficients loaded" << endl;
        cout << "Camera matrix : " << cameraMatrix << endl;
        cout << "Distortion coefficients : " << distCoeffs << endl;
        cout << "Undistording the image ..." << endl;
    }
    undistort(image, undistorded, cameraMatrix, distCoeffs);
    if (PRINT)
        cout << "Image undistorted" << endl;

    // display the image
    if (DISPLAY)
    {
        namedWindow("Undistorded Image", WINDOW_NORMAL);
        resizeWindow("Undistorded Image", 1920, 1080);
        imshow("Undistorded Image", undistorded);
        waitKey(0);
        destroyAllWindows();
    }

    return 0;
}

int crop_image(Mat &image, Mat &cropped, const Point2i UPPER_LEFT, const Point2i LOWER_RIGHT, const bool DISPLAY = true, const bool PRINT = true)
{

    if (PRINT)
        cout << "Croping the image ..." << endl;

    cropped = image(Range(UPPER_LEFT.y, LOWER_RIGHT.y), Range(UPPER_LEFT.x, LOWER_RIGHT.x));

    if (PRINT)
        cout << "Image cropped" << endl;

    // Display the cropped image
    if (DISPLAY)
    {
        namedWindow("Cropped Image", WINDOW_NORMAL);
        resizeWindow("Cropped Image", 1920, 1080);
        imshow("Cropped Image", cropped);
        waitKey(0);
        destroyAllWindows();
    }

    return 0;
}

Scalar ScalarRGB2HSV(uchar R, uchar G, uchar B)
{
    /**
     * Converts a RGB code to a HSV values
     * 
     * @param  {uchar} R : red value
     * @param  {uchar} G : green value
     * @param  {uchar} B : blue value
     * @return {Scalar} : HSV value corresponding to the rgb input
     */

    Mat hsv;
    Mat rgb(1, 1, CV_8UC3, Scalar(R, G, B));
    cvtColor(rgb, hsv, COLOR_RGB2HSV);
    return Scalar(hsv.data[0], hsv.data[1], hsv.data[2]);
}

int hsv_threshold(Mat &image, Mat &thresholded_image, const Scalar COLOR, const int VAR_H, const int VAR_S, const int VAR_V, const bool DISPLAY = true, const bool PRINT = true)
{
    /**
     *  Operates a HSV threshold to the image and returns the thresholded image
     * 
     * @param  {Mat} image             : in image to be thresholded
     * @param  {Mat} thresholded_image : image thresholded
     * @param  {Scalar} COLOR          : color to be thresholded
     * @param  {int} VAR_H             : variance of the hue
     * @param  {int} VAR_S             : variance of the saturation
     * @param  {int} VAR_V             : variance of the value
     * @param  {bool} DISPLAY          : choose to DISPLAY or not the thresholded image
     * @param  {bool} PRINT            : choose to PRINT or not informations during the process
     * @return {int}                   : error code if error -1, else 0
     */

    Mat hsv_image;
    // Convert  image to HSV --> hsv_image
    if (PRINT)
        cout << "Converting image to HSV" << endl;
    cvtColor(image, hsv_image, COLOR_BGR2HSV);

    // Convert Scalar COLOR to HSV
    if (PRINT)
        cout << "Converting Scalar COLOR to HSV" << endl;
    Scalar HSV_COLOR = ScalarRGB2HSV(COLOR[0], COLOR[1], COLOR[2]);

    // Threshold HSV image
    if (PRINT)
        cout << "Thresholding HSV image" << endl;
    inRange(hsv_image, Scalar(HSV_COLOR[0] - VAR_H, HSV_COLOR[1] - VAR_S, HSV_COLOR[2] - VAR_V), Scalar(HSV_COLOR[0] + VAR_H, HSV_COLOR[1] + VAR_S, HSV_COLOR[2] + VAR_V), thresholded_image);

    // displays the thresholded image
    if (DISPLAY)
    {
        namedWindow("Threshold", WINDOW_NORMAL);
        resizeWindow("Threshold", 1920, 1080);
        imshow("Threshold", thresholded_image);
        waitKey(0);
        destroyAllWindows();
    }
    return 0;
}

int filter_image(Mat &image, Mat &filtered, const int KERNEL_SIZE, const int ITERATIONS, const bool DISPLAY = true, const bool PRINT = true, const int TYPE = MORPH_CLOSE)
{
    /**
     * Filters the image with a morphological filter. (usually a closing)
     * 
     * @param  {Mat} image              : in image to be filtered
     * @param  {Mat} filtered           : out image filtered 
     * @param  {int} KERNEL_SIZE        : kernel size of the filter
     * @param  {int} ITERATIONS         : number of iterations of the filter
     * @param  {bool} DISPLAY           : choose to DISPLAY or not the filtered image
     * @param  {bool} PRINT             : choose to PRINT or not informations during the process
     * @param  {int} TYPE = MORPH_CLOSE : TYPE of the filter
     * @return {int}                    : 
     */

    // define the kernel
    if (PRINT)
        cout << "Creating the kernel..." << endl;
    Mat KERNEL = getStructuringElement(MORPH_RECT, Size(KERNEL_SIZE, KERNEL_SIZE));
    if (PRINT)
        cout << "Kernel created" << endl;

    // apply the filter
    if (PRINT)
        cout << "Applying the closing..." << endl;
    morphologyEx(image, filtered, TYPE, KERNEL, Point(-1, -1), ITERATIONS);
    if (PRINT)
        cout << "Closing applied" << endl;

    // DISPLAY the filtered image
    if (DISPLAY)
    {
        namedWindow("Filtered image", WINDOW_NORMAL);
        resizeWindow("Filtered image", 1920, 1080);
        imshow("Filtered image", filtered);
        waitKey(0);
        destroyAllWindows();
    }

    return 0;
}
int detect_circles(vector<Vec3f> &centers, Mat &image_in, Mat &image_out, const int MIN_RADIUS, const int MAX_RADIUS, const int MAX_NUMBER, const double PARAM1, const double PARAM2, const bool DISPLAY = true, const bool PRINT = true)
{

    /**
     * detect the circles in the image and returns the centers of the circles
     * 
     * @param  {vector<Vec3f>} centers : in-out vector of the detected circles [(x,y,radius,vote), ...] votes not used
     * @param  {Mat} image_in          : image withing the circles are detected
     * @param  {int} MIN_RADIUS        : minimum radius of the circles
     * @param  {int} MAX_RADIUS        : maximum radius of the circles
     * @param  {int} MAX_NUMBER        : number of circles to be detected
     * @param  {double} PARAM1         : parameter of the canny edge gradient (not very important because the image is Black and White)
     * @param  {double} PARAM2         : parameter of the quality of the detected circles
     * @param  {bool} DISPLAY          : choose to DISPLAY or not the image with the detected circles
     * @param  {bool} PRINT            : choose to PRINT or not informations during the process
     * @return {int}                   : code of the error if error -1, else 0
     */

    // Reset the center vector just in case
    centers.clear();

    // Converts the image to be displayed to BGR
    cvtColor(image_in, image_out, COLOR_GRAY2BGR);

    // Detect the circles
    if (PRINT)
        cout << "Detecting circles..." << endl;
    HoughCircles(image_in, centers, HOUGH_GRADIENT, 1, 2 * MIN_RADIUS, PARAM1, PARAM2, MIN_RADIUS, MAX_RADIUS);

    // Count the number of circles and act accordingly
    if (PRINT)
        cout << "Found " << centers.size() << " circles" << endl;
    if (centers.size() > MAX_NUMBER)
    {
        cerr << "Too many circles detected!\n"
             << endl;
        return -1;
    }

    // Draw the circles and DISPLAY the image
    if (DISPLAY)
    {
        // Draw circles
        if (PRINT)
            cout << "Drawing circles..." << endl;
        for (size_t i = 0; i < centers.size(); i++)
        {
            Point draw_center(cvRound(centers[i][0]), cvRound(centers[i][1]));
            int radius = cvRound(centers[i][2]);
            circle(image_out, draw_center, radius, Scalar(0, 255, 0), 3, LINE_AA);
            drawMarker(image_out, draw_center, Scalar(0, 0, 255), MARKER_CROSS, 40, 8);
            // centers.push_back(draw_center);
        }
        if (PRINT)
            cout << "Circles drawn" << endl;
        {
            namedWindow("Circle detection", WINDOW_NORMAL);
            resizeWindow("Circle detection", 1920, 1080);
            imshow("Circle detection", image_out);
            waitKey(0);
            destroyAllWindows();
        }
    }
    return 0;
}

int find_arrival(Point2f &arrival, const Point2f A, const Point2f B, const Point2f C, const Point2f D)
{
    /**
     * Find the arrival coordinates of the ball
     * 
     * @param  {Point2f} arrival : out arrival time
     * @param  {Point2f} A       : in time of the first detection
     * @param  {Point2f} B       : in time of the second detection
     * @param  {Point2f} C       : line arrival
     * @param  {Point2f} D       : line arrival
     * @return {int}            : code of the error if error -1, else 0
     */

    // A1  = XA

    // // float A1 = A.x - B.y;
    // // float B1 = B.x - A.x;
    // // float C1 = A.x * B.y - A.y * B.x;
    // // float A2 = C.y - D.y;
    // // float B2 = D.x - C.x;
    // // float C2 = C.x * D.y - C.y * D.x;

    // // // xm = C2 B1 - C1 B2 / A1 B2 - A2 B1
    // // arrival.x = (C2 * B1 - C1 * B2) / (A1 * B2 - A2 * B1);
    // // // ym = C2 A1 - C1 A2 / A2 B1 - A1 B2
    // // arrival.y = (C2 * A1 - C1 * A2) / (A2 * B1 - A1 * B2);

    float x1, x2, x3, x4, y1, y2, y3, y4;
    x1 = A.x;
    x2 = B.x;
    x3 = C.x;
    x4 = D.x;
    y1 = A.y;
    y2 = B.y;
    y3 = C.y;
    y4 = D.y;

    // // source https://en.wikipedia.org/wiki/Line%E2%80%93line_intersection
    // float t =  ((x1-x3)*(y1-y2)-(y1-y3)*(x1-x2))/((x1-x2)*(y3-y4)-(y1-y2)*(x3-x4));
    // arrival.x = x1 + t*(x2-x1);
    // arrival.y = y1 + t*(y2-y1);

    float Det = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);

    arrival.x = ((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)) / Det;
    arrival.y = ((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)) / Det;

    return 0;
}

//TODO TESTING... fabrication images sur la rpi etc


void gauche()
{
    digitalWrite(IN1_PIN, HIGH);
    digitalWrite(IN2_PIN, LOW);
}
// rotation dans l'autre sens
void droite()
{
    digitalWrite(IN1_PIN, LOW);
    digitalWrite(IN2_PIN, HIGH);
}
// arret du moteur
void stop()
{
    digitalWrite(IN1_PIN, LOW);
    digitalWrite(IN2_PIN, LOW);
}

// reglage de la vitesse sur une echelle de 0 a 1024
void vitesse(int vitesse)
{
    if (vitesse >= 0 && vitesse < 1024)
    {
        pwmWrite(EN_PIN, vitesse);
    }
    else if (vitesse >= 1024)
    {
        pwmWrite(EN_PIN, 1024);
    }
    else
    {
        pwmWrite(EN_PIN, 0);
    }
}

// FONCTIONS POUR LE DEPLACEMENT

int get_pos_actuelle()
{
    int pos_actuelle;
    pos_actuelle = 1000; // normalement a determiner par la vision
    return pos_actuelle;
}

int get_pos_desiree()
{
    int pos_desiree;
    pos_desiree = 500; // normalement determinee par la prevision de trajectoire
    return pos_desiree;
}

// FONCTION POUR INITIALISER LE MOTEUR

void init_wirinPi()
{
    wiringPiSetup();
    // parametrisation des pins
    pinMode(EN_PIN, PWM_OUTPUT);
    pinMode(IN1_PIN, OUTPUT);
    pinMode(IN2_PIN, OUTPUT);
    // initialisation du sens de rotation et de la vitesse
    stop();
    vitesse(0);
}

// fonction grossiere avant de faire un PID
void bouger_position()
{
    int k = 0;
    int nb_iter_max = 20; // pour quitter la boucle while quand on atteint un nb max d'iterations
    int pas = 20;         // erreur toleree entre la consigne et la position de la boite
    int tps = 100;        // temps durant lequel les moteurs sont actives
    int position = get_pos_actuelle();
    int consigne = get_pos_desiree();
    int erreur = position - consigne;

    // boucle qui actionne le moteur dans le sens desire tant que
    // la cage n'est pas dans la bonne position, a une erreur pres appelee "pas"
    while (abs(erreur) >= pas && k < nb_iter_max)
    {
        if (erreur > 0)
        {
            droite();
            delay(tps);
            stop();
            k = k + 1;
            // valeurs factices pour voir si la fonction fonctionne
            position = position - 100;
            // position = get_pos_actuelle() ;
            erreur = position - consigne;
        }
        else if (erreur < 0)
        {
            gauche();
            delay(tps);
            stop();
            k = k + 1;
            // valeurs factices pour voir si la fonction fonctionne
            position = position + 100;
            // position = get_pos_actuelle() ;
            erreur = position - consigne;
        }
        else
        {
            stop();
            cout << "euh je ne sais pas ";
        }
    }
}

float compute_distance(float &dist, Mat &im, Point2f Ma, Point2f Mb, const Point2f A, const Point2f B)
{
    /**
     * Calculate the distance between the arrival point and current position,
     * scales from 0 to 1
     * 
     * @param  {float} dist : out distance
     * @param  {Point2f} Ma  : in first point aruco
     * @param  {Point2f} Mb  : in second point marble
     * @param  {Point2f} A   : in first point of the line
     * @param  {Point2f} B   : in second point of the line
     * @return {int}        : code of the error if error -1, else 0
     */

    float lambda; // coeff ma
    float mu;     // coeff mb

    // calcul des coeffs lambda et mu par projete orthogonal (prdoduit scalaire)

    lambda = ((B.x - A.x) * (Ma.x - A.x) + (B.y - A.y) * (Ma.y - A.y)) / ((B.x - A.x) * (B.x - A.x) + (B.y - A.y) * (B.y - A.y));

    mu = ((B.x - A.x) * (Mb.x - A.x) + (B.y - A.y) * (Mb.y - A.y)) / ((B.x - A.x) * (B.x - A.x) + (B.y - A.y) * (B.y - A.y));

    Point2f imMa = Point2f(A.x + lambda * (B.x - A.x), A.y + lambda * (B.y - A.y));
    Point2f imMb = Point2f(A.x + mu * (B.x - A.x), A.y + mu * (B.y - A.y));

    drawMarker(im, imMa, Scalar(255, 255, 0), MARKER_CROSS, 40, 8);
    drawMarker(im, imMb, Scalar(255, 0, 255), MARKER_CROSS, 50, 6);

    // calcul de la distance
    dist = lambda - mu;
    return 0;
}

