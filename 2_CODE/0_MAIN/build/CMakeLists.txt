cmake_minimum_required(VERSION 2.8)
project( main )
find_package( OpenCV REQUIRED )
find_library(wiringPi_LIB wiringPi)

# Include Opencv and my own header
include_directories( ${OpenCV_INCLUDE_DIRS} ${CMAKE_CURRENT_SOURCE_DIR} )
set(CMAKE_CXX_STANDARD 17)
add_executable( main ../main.cpp ../utils.cpp ../Motor.cpp)
target_link_libraries( main ${OpenCV_LIBS} stdc++fs)
target_link_libraries(main ${wiringPi_LIB})

set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)
target_link_libraries(main PRIVATE Threads::Threads)


