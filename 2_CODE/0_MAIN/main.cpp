#include <iostream>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/aruco.hpp>
#include <string>
#include "utils.hpp"
#include <chrono>
#include <sys/time.h>
#include <ctime>
#include <unistd.h>
#include <cstdlib>
#include <signal.h>
#include "Motor.cpp"
#include <filesystem>
#include <thread> // std::thread

using namespace cv;
using namespace std;
using namespace aruco;
using std::chrono::duration_cast;
using std::chrono::milliseconds;
using std::chrono::seconds;
using std::chrono::system_clock;

static volatile int keepRunning = 1;

// Define the function to be called when ctrl-c (SIGINT) is sent to process
void signal_callback_handler(int signum)
{
    keepRunning = false;
}

int main()
{
    // Empties the directory
    filesystem::remove_all("../out/");
    filesystem::create_directory("../out/");

    // Register signal and signal handler
    signal(SIGINT, signal_callback_handler);
    // Wrtiting output to files
    const bool WRITE = true;
    vector<Mat> images;

    // --- ARUCO ---
    // opencv frames
    Mat frame;
    Mat frame_gray;
    Mat undistorded;
    Mat cropped;
    Mat cropped_display;

    bool aruco_detected = false;

    // aruco
    Ptr<aruco::Dictionary> dictionary = aruco::getPredefinedDictionary(aruco::DICT_6X6_250);
    Ptr<aruco::DetectorParameters> detectorParams = aruco::DetectorParameters::create();
    vector<vector<Point2f>> markerCorners, rejectedCandidates;
    vector<int> markerIds;
    vector<Vec3d> rvecs, tvecs;
    const float ARUCO_SIZE = 0.050; // in meters
    // camera parameters
    Mat cam_matrix, dist_coeffs;
    const string camera_file_params_file = "../../1_CAMERA_UTILS/calibration_rpi.yml";

    // --- MARBLE ---
    // Cropping parameters
    const Point2i CROP_UP_LEFT(200, 62);
    const Point2i CROP_DOWN_RIGHT(1100, 639);

    // Thresholds parameters
    // colors
    //todo trouver les bonnes couleurs
    const Scalar MARBLE_COLOR = Scalar(74, 127, 111);      //BGR  Balle verte RGB 24, 71, 62  RGB 30, 68, 64
    const Scalar BACKGROUND_COLOR = Scalar(182, 160, 119); //BGR
    const Scalar MARBLE_COLOR_2 = Scalar(30, 30, 41);      //BGR
    const Scalar BARS_COLOR = Scalar(130, 33, 56);         //BGR
    // Variance HSV
    //todo trouver les bonnes valeurs
    const int VAR_H = 15;
    const int VAR_S = 60;
    const int VAR_V = 60;

    // Masks Matrices to be computed
    Mat mask_marble;     // mask for marble
    Mat mask_marble_2;   // mask for marble 2
    Mat mask_background; // mask for background
    Mat mask_bars;       // mask for bars

    // Filters parameters
    Mat filtered_image;         // filtered image
    const int KERNEL_SIZE = 20; // kernel size for the closing kernel
    const int ITERATIONS = 1;   // number of iterations for the closing kernel to be operated

    // Circles parameters
    Mat circle_detection_image;           // image to be used for circle detection
    vector<Vec3f> marbles_centers;        // centers of circles
    const int MARBLE_NUMBER = 1;          // max number of circles to be detected (ie nombre max de bille simultanee)
    const int MARBLE_MIN_RADIUS = 13 - 3; // radius of circles
    const int MARBLE_MAX_RADIUS = 13 + 3; // radius of circles
    const double PARAM1 = 300;            // peu d'importance
    const double PARAM2 = 4;              // 4..16    4 <-- bcp de cercle detecte ; 16 <-- peu de cercle detecte
    bool marble_detected_twice = false;   // true if a marble is detected

    // Scalar for arrival
    Point2f A = Point2f(0, 0);
    Point2f B = Point2f(0, 0);

    // CD define the arrival line
    Point2f C = Point2f(92, 553);
    Point2f D = Point2f(89, 21);

    Point2f Mb = Point2f(0, 0); // Arival point
    Point2f current_center;     // currrent center of the marble --> B

    Point2f Ma = Point2f(0, 0); // Point of aruco pixels

    int millisec_since_epoch_t0;
    int millisec_since_epoch_tf;

    long frame_count = 0;

    // --- MOTORS ---
    float consigne_value = 0;
    bool consigne_active = false;

    // --- INITIALIZATION ---
    // motors
    Motor motJaune = Motor();
    thread thread_motor = thread(&Motor::run, &motJaune); // Pass 10 to member function

    // Camera
    cout << "Loading camera parameters..." << endl;
    load_camera_params(camera_file_params_file, cam_matrix, dist_coeffs);

    VideoCapture cap;
    int device = 0;
    int api = CAP_ANY;

    // //! Verifier que ca fonctionne sur la RPi 1920x1080 normalement
    // const vector<int> params = {CAP_PROP_FRAME_WIDTH, 1920, CAP_PROP_FRAME_HEIGHT, 1088};
    const vector<int> params = {CAP_PROP_FRAME_WIDTH, 1280, CAP_PROP_FRAME_HEIGHT, 720, CAP_PROP_BUFFERSIZE, 1, CAP_PROP_FPS, 10};
    cap.open(device, api, params);
    // check if we succeeded
    for (int i = 0; i < 10; i++)
    {
        if (!cap.isOpened())
        {
            cerr << "ERROR! Unable to open camera\n";
            return -1;
        }

        cap.read(frame);
    }


    cout << "Press enter to start" << endl;
    cin.get();

    // ------------------------ MAIN LOOP ---------------------------------

    while (keepRunning)
    {
        frame_count++;
        millisec_since_epoch_t0 = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
        // First, we get the frame
        cap.read(frame);
        // check if we succeeded
        if (frame.empty())
        {
            cerr << "ERROR! blank frame grabbed\n";
            // break;
        }
        millisec_since_epoch_tf = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
        // cout << "Took picture, time = " << millisec_since_epoch_tf - millisec_since_epoch_t0 << " ms" << endl;

        // Then, we undistort the frame
        undistorded = frame.clone();

        // --- Marble detection ---

        // undistort_image(frame, undistorded, cam_matrix, dist_coeffs, false, false);
        // millisec_since_epoch_tf = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
        // cout << "Undistort, time = " << millisec_since_epoch_tf - millisec_since_epoch_t0 << " ms" << endl;

        crop_image(frame, cropped, CROP_UP_LEFT, CROP_DOWN_RIGHT, false, false);
        millisec_since_epoch_tf = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
        // cout << "Crop, time = " << millisec_since_epoch_tf - millisec_since_epoch_t0 << " ms" << endl;

        hsv_threshold(cropped, mask_marble, MARBLE_COLOR, VAR_H, VAR_S, VAR_V, false, false);
        millisec_since_epoch_tf = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
        // cout << "Threshold, time = " << millisec_since_epoch_tf - millisec_since_epoch_t0 << " ms" << endl;
        cropped_display = cropped.clone();

        filter_image(mask_marble, filtered_image, KERNEL_SIZE, ITERATIONS, false, false, MORPH_CLOSE);
        millisec_since_epoch_tf = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
        // cout << "Filter, time = " << millisec_since_epoch_tf - millisec_since_epoch_t0 << " ms" << endl;

        detect_circles(marbles_centers, filtered_image, circle_detection_image, MARBLE_MIN_RADIUS, MARBLE_MAX_RADIUS, MARBLE_NUMBER, PARAM1, PARAM2, false, false);
        millisec_since_epoch_tf = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
        // cout << "Marble detetted, time = " << millisec_since_epoch_tf - millisec_since_epoch_t0 << " ms" << endl;

        //draw circles
        cout << "Marble detected : " << marbles_centers.size() << endl;
        for (size_t i = 0; i < marbles_centers.size(); i++)
        {
            Point draw_center(cvRound(marbles_centers[i][0]), cvRound(marbles_centers[i][1]));
            current_center = Point2f(marbles_centers[i][0], marbles_centers[i][1]);
            int radius = cvRound(marbles_centers[i][2]);

            B = current_center;
            if (A != Point2f(0, 0))
            {
                find_arrival(Mb, A, B, C, D);
                // Mb = Point2f(89, 300);
            }
            else
            {
                cout << "Previous point not defined" << endl;
            }

            // cout << "Mb : " << Mb << endl;

            if (WRITE)
            {
                circle(cropped_display, draw_center, radius, Scalar(0, 255, 0), 3, LINE_AA);
                drawMarker(cropped_display, draw_center, Scalar(0, 0, 255), MARKER_CROSS, 40, 8);
                drawMarker(cropped_display, Point2f(Mb.x, Mb.y), Scalar(255, 0, 0), MARKER_CROSS, 40, 8);
                drawMarker(cropped_display, A, Scalar(0, 0, 255), MARKER_CROSS, 40, 8);
                drawMarker(cropped_display, C, Scalar(0, 255, 0), MARKER_CROSS, 40, 8);
                drawMarker(cropped_display, D, Scalar(0, 255, 0), MARKER_CROSS, 40, 8);
            }
            // centers.push_back(draw_center);
        }
        if (marbles_centers.size() == 0)
            current_center = Point2f(0, 0);
        if (A != Point2f(0, 0) && current_center != Point2f(0, 0))
            marble_detected_twice = true;
        else
            marble_detected_twice = false;
        // --- End of marble detection ---

        // --- Aruco detection ---
        cvtColor(cropped, frame_gray, COLOR_BGR2GRAY);
        detect_markers(frame_gray, cam_matrix, ARUCO_SIZE, dist_coeffs, dictionary, detectorParams, markerCorners, rvecs, tvecs, markerIds, rejectedCandidates);

        // Draws the markers
        millisec_since_epoch_tf = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
        cout << "Markers detected : " << markerIds.size() << endl;
        // cout << "Markers detected, time = " << millisec_since_epoch_tf - millisec_since_epoch_t0 << " ms" << endl;
        if (markerIds.size() > 0)
        {
            aruco_detected = true;
            if (WRITE)
                drawDetectedMarkers(cropped, markerCorners, markerIds);
            Point2f cent(0, 0);
            for (int i = 0; i < markerIds.size(); i++)
            {
                for (int p = 0; p < 4; p++)
                    cent += markerCorners[i][p];
                cent = cent / 4.;
                Ma = cent;
            }
            // Draws the center of the markers
            if (WRITE)
                circle(cropped_display, cent, 5, Scalar(0, 255, 0), 3, LINE_AA);
        }
        else
            aruco_detected = false;

        millisec_since_epoch_tf = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
        cout << "Time elapsed for the frame " << frame_count << " : " << millisec_since_epoch_tf - millisec_since_epoch_t0 << " ms" << endl;
        // cout << "Frame rate =" << 1000. / (millisec_since_epoch_tf - millisec_since_epoch_t0) << endl;

        // if possible compute the consigne_value
        if (aruco_detected && marble_detected_twice)
        {
            // Computes the consigne_value
            compute_distance(consigne_value, cropped_display, Ma, Mb, D, C);
            consigne_active = true;
        }
        else
            consigne_active = false;

        // cout << "Ma" << Ma << endl;

        // cout << "Mb" << Mb << endl;

        // cout << "Consigne Value : " << consigne_value << endl;

        if (consigne_active)
            motJaune.set_dist(consigne_value);
        else
            motJaune.set_dist(0);

        cout << " Consigne active : " << consigne_active << endl;

        cout << "-----------------------" << endl;
        // Prev center = current center

        // Next iteration
        A = current_center;
        images.push_back(cropped_display.clone());
    }

    motJaune.stop_motor();
    thread_motor.join();
    cout << "Saving images and exiting" << endl;
    for (int i = 0; i < images.size(); i++)
    {
        // saves the image
        imwrite("../out/frame" + to_string(i) + ".jpg", images[i]);
    }

    return 0;
}