#include <iostream>
#include "utils.hpp"
// #include "utils.cpp"
using namespace std;

class Motor
{
private:
    float dist;
    float prev_dist = 0;
    float prev_prev_dist = 0;
    float prev_prev_prev_dist = 0;
    float prev_prev_prev_prev_dist = 0;
    // float prev_prev_prev_prev_prev_dist = 0;
    float Kp;
    float Ki;
    float Kd;
    float offset;
    bool enabled;
    float speed;
    int speed_i = 0;
    float integral = 0;

public:
    Motor(float distance, float Kp, float Ki, float Kd, float offset)
    {
        this->dist = distance;
        this->Kp = Kp;
        this->Kd = Kd;
        this->enabled = false;
        this->speed = 0;
        // init
        wiringPiSetup();
        // parametrisation des pins
        pinMode(EN_PIN, PWM_OUTPUT);
        pinMode(IN1_PIN, OUTPUT);
        pinMode(IN2_PIN, OUTPUT);
        // initialisation du sens de rotation et de la vitesse
        stop();
        vitesse(0);
    }

    Motor()
    {
        dist = 0;
        Kp = 200;
        // Kp = 0;
        Ki = 10;
        // Kd = 150;
        Kd = -150;
        offset = 400;
        enabled = false;
        speed = 0;

        // init
        wiringPiSetup();
        // parametrisation des pins
        pinMode(EN_PIN, PWM_OUTPUT);
        pinMode(IN1_PIN, OUTPUT);
        pinMode(IN2_PIN, OUTPUT);
        // initialisation du sens de rotation et de la vitesse
        stop();
        vitesse(0);
    }

    void run()
    {
        cout << "starting motor" << endl;
        enabled = true;
        while (enabled)
        {
            speed = dist * Kp + Ki * integral + Kd * (prev_dist - dist);
            // speed = Ki * integral;
            // speed = abs(1024);

            if (speed > 0)
            {
                droite();
                // if (dist - prev_dist > 0)
                // {
                //     speed = speed + Kd * (dist - prev_dist);
                // }
            }
            else if (speed == 0)
            {
                stop();
            }
            else
            {
                gauche();
                // if (dist - prev_dist < 0)
                //     speed = speed + Kd * (dist - prev_dist);
            }

            speed = abs(speed);
            speed = speed + offset;

            speed_i = (int)speed;
            vitesse(speed_i);
        }
        cout << "stopping motor" << endl;
    }

    void set_dist(float distance_recue)
    {
        prev_dist = dist;
        prev_prev_dist = prev_dist;
        prev_prev_prev_dist = prev_prev_dist;
        prev_prev_prev_prev_dist = prev_prev_prev_dist;
        // prev_prev_prev_prev_prev_dist = prev_prev_prev_prev_dist;
        this->dist = -distance_recue;
        integral = dist + prev_dist + prev_prev_dist + prev_prev_prev_dist + prev_prev_prev_prev_dist;


        cout << "Consigne set to " << speed_i << endl;
        cout << "Integral term set to " << integral * Ki << endl;
        cout << "Proportional term set to " << dist * Kp << endl;
        cout << "Derivative term set to " << Kd * (prev_dist - dist) << endl;
    }

    void stop_motor()
    {
        enabled = false;
        stop();
    }
};
