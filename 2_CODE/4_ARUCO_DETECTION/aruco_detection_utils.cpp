#include "aruco_detection_utils.hpp"


int detect_markers(Mat &frame, Mat &cam_matrix, const float &ARUCO_SIZE,Mat &dist_coeffs, Ptr<aruco::Dictionary> &dictionary, Ptr<aruco::DetectorParameters> parameters, vector<vector<Point2f>> &corners, vector<Vec3d> &rvecs, vector<Vec3d> &tvecs, vector<int> &ids, vector<vector<Point2f>> rejectedCandidates)
{
    aruco::detectMarkers(frame, dictionary, corners, ids, parameters, rejectedCandidates);
    
    if(ids.size() > 0)
    {
        
        aruco::estimatePoseSingleMarkers(corners, ARUCO_SIZE, cam_matrix, dist_coeffs, rvecs, tvecs);
    }
    return 0;
}

int load_camera_params(string filename, Mat &cam_matrix, Mat &dist_coeffs)
{
    FileStorage fs(filename, FileStorage::READ);
    if(!fs.isOpened())
    {
        cout << "Failed to open file " << filename << endl;
        return -1;
    }
    fs["cameraMatrix"] >> cam_matrix;
    fs["distCoeffs"] >> dist_coeffs;
    cout << "Camera matrix: " << cam_matrix << endl;
    cout << "Distortion coefficients: " << dist_coeffs << endl;
    return 0;
}
