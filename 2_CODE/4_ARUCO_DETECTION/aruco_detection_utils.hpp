#pragma once

#include <iostream>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/aruco.hpp>
#include <string>

using namespace cv;
using namespace std;

int load_camera_params(string filename, Mat &cam_matrix, Mat &dist_coeffs);

int detect_markers(Mat &frame, Mat &cam_matrix, const float &ARUCO_SIZE,Mat &dist_coeffs, Ptr<aruco::Dictionary> &dictionary, Ptr<aruco::DetectorParameters> parameters, vector<vector<Point2f>> &corners, vector<Vec3d> &rvecs, vector<Vec3d> &tvecs, vector<int> &ids, vector<vector<Point2f>> rejectedCandidates);
