#include "aruco_detection_utils.hpp"

using namespace cv;
using namespace std;



int main(){

    // Loads the camera calibration
    Mat cam_matrix, dist_coeffs;
    string camera_file_params_file = "../../1_CAMERA_UTILS/calibration_rpi.yml";
    load_camera_params(camera_file_params_file, cam_matrix, dist_coeffs);

    // Creates the aruco variables
    Ptr<aruco::Dictionary> dictionary = aruco::getPredefinedDictionary(aruco::DICT_6X6_250);
    Ptr<aruco::DetectorParameters> parameters = aruco::DetectorParameters::create();

    vector<int> markerIds;
    vector<vector<Point2f>> markerCorners, rejectedCandidates;
    const float ARUCO_SIZE = 0.050; // in meters
    vector<Vec3d> rvecs, tvecs;



    // Frames 
    Mat frame;
    Mat frame_gray;


    // Loads the image
    cout << "Loading image" << endl;
    frame = imread("../../1_CAMERA_UTILS/oneImage/oneImage.jpg");
    cout << "Image loaded" << endl;
    cvtColor(frame, frame_gray, COLOR_BGR2GRAY);
    // detect_markers(frame_gray, cam_matrix, ARUCO_SIZE, dist_coeffs, dictionary, parameters, markerCorners, markerIds, markerCorners, markerIds, rejectedCandidates);
    detect_markers(frame_gray, cam_matrix, ARUCO_SIZE, dist_coeffs, dictionary, parameters, markerCorners, rvecs, tvecs, markerIds, rejectedCandidates);

    // Draws the markers
    aruco::drawDetectedMarkers(frame, markerCorners, markerIds);
    aruco::drawAxis(frame, cam_matrix, dist_coeffs, rvecs, tvecs, ARUCO_SIZE);

    // Saves the image
    cout << "Saving image" << endl;
    imwrite("../../1_CAMERA_UTILS/oneImage/oneImage_detected.jpg", frame);
    cout << "Image saved" << endl;

    cout << "tvecs : "<< endl;
    for(int i = 0; i < tvecs.size(); i++){
        cout << tvecs[i] << endl;
    }
    return 0;

}
