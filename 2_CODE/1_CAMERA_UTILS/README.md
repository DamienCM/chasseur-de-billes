# Camera Utils

## Objectifs 
Obtenir des images de facon facile avec ce package

## Fonctions 
Fonctions :
    - get_image()
    - crop()
        Rogne l'image sur la zone intéressante
    - resize()
        Redimensionne l'image pour ne pas surcharger les calculs
    - eagle_view()
        Permet d'obtenir une vue dessus parfaite