#include <opencv2/aruco.hpp>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/aruco/charuco.hpp>
#include <iostream>
#include <vector>
#include <string>
#include <sys/stat.h>
#include <filesystem>
#include <chrono>
#include <thread>
#include <unistd.h>

using namespace cv;
using namespace std;

namespace fs = std::filesystem;

int select_directory(string dir)
{
    // Checks if the directory exists
    bool isDir = fs::is_directory(dir);
    if (!isDir)
    {
        cerr << "Creating directory : " + dir << endl;
        fs::create_directory(dir);
    }
    else
    {
        cerr << "Directory selected" << endl;
    }
    return 0;
}

int detect_charuco(const String directory, const bool display, const bool print, vector<vector<Point2f>> &allCharucoCorners, vector<vector<int>> &allCharucoIds)
{

    cout << "Detecting charuco board in " + directory << std::endl;
    int i = 0;
    string filename;
    Mat image;

    // Create Aruco dictionary and >checkboard
    const Ptr<aruco::Dictionary> dictionary = getPredefinedDictionary(aruco::DICT_5X5_50);
    const Ptr<aruco::CharucoBoard> charucoBoard = aruco::CharucoBoard::create(7, 7, 0.02f, 0.01f, dictionary);

    // Detect aruco stored theses vectors
    vector<vector<Point2f>> markerCorners, rejectedCorners;
    vector<int> markerIds;

    // parameters for detection
    const Ptr<aruco::DetectorParameters> params = aruco::DetectorParameters::create();
    params->cornerRefinementMethod = aruco::CORNER_REFINE_CONTOUR;
    params->cornerRefinementWinSize = 31;

    vector<cv::Point2f> charucoCorners; // detected charuco corners
    vector<int> charucoIds;             // charuco corner ids

    // File names
    for (const auto &entry : fs::directory_iterator(directory))
    {
        filename = entry.path();
        if (print)
            cout << "Reading : " + filename << std::endl;

        image = imread(filename);

        aruco::detectMarkers(image, dictionary, markerCorners, markerIds, params, rejectedCorners);

        if (!markerIds.empty())
        {
            aruco::interpolateCornersCharuco(markerCorners, markerIds, image, charucoBoard, charucoCorners, charucoIds);
            allCharucoCorners.push_back(charucoCorners);
            allCharucoIds.push_back(charucoIds);
            aruco::drawDetectedCornersCharuco(image, charucoCorners, charucoIds);
            if (print)
                cout << "Accepted" << endl;
            if (display)
            {
                imshow(directory, image);
                waitKey(500);
            }
        }
        else
        {
            cout << "Deleted" << endl;
            fs::remove(filename);
        }
    }
    if (print)
        cout << "Detection : Done" << endl;
    return -1;
}

int calibrate(const String directory) // 25.8 mm square size
{
    // Creating the used charuco board for the pictures
    const Ptr<aruco::Dictionary> dictionary = getPredefinedDictionary(aruco::DICT_5X5_50);

    const Ptr<aruco::CharucoBoard> charucoBoard = aruco::CharucoBoard::create(7, 7, .0258f, .013f, dictionary);

    vector<vector<Point2f>> allCharucoCorners;
    vector<vector<int>> allCharucoIds;

    // image size
    Size imageSize = Size(1920, 1088);
    // Camera parameters
    Mat cameraMatrix, distCoeffs;
    // calculate corners and ids from charuco board
    detect_charuco(directory, false, false, allCharucoCorners, allCharucoIds);
    // extract camera paramaters
    calibrateCameraCharuco(allCharucoCorners, allCharucoIds, charucoBoard, imageSize, cameraMatrix, distCoeffs);
    cout << "Calibration : Done" << endl;
    // save camera matrix and distortion coefficients as yml file
    FileStorage fs("calibration.yml", FileStorage::WRITE);
    fs << "cameraMatrix" << cameraMatrix;
    fs << "distCoeffs" << distCoeffs;
    fs.release();
    cout << "Calibration saved as : calibration.yml" << endl;

    return 0;
}

int save_pictures(const string directory)
{
    /* Take a pictures from the camera and save it in the folder in input 
    loop until esc is presssed
    */
    cout << "Taking pictures ..." << endl
         << "Press esc to quit";

    VideoCapture cap;
    int device = 0;
    int api = CAP_ANY;

    // //! Verifier que ca fonctionne sur la RPi 1920x1080 normalement
    const vector<int> params = {CAP_PROP_FRAME_WIDTH, 1920, CAP_PROP_FRAME_HEIGHT, 1088};
    cap.open(device, api, params);

    Mat image;
    int k;
    int i = 0;
    if (!cap.isOpened())
    {
        cout << "Cannot open the video cam" << endl;
        return -1;
    }
    // Photo loop
    for (;;)
    {
        i++;
        cap.read(image);
        if (image.empty())
        {
            cerr << "No image retrieved" << endl;
            return -1;
        }
        imshow("image", image);
        k = waitKey(2000); // wait for 2 seconds before taking a new picture
        if (k == 27)       // esc is pressed
            break;
        // saves the picture to the calibration_images folder
        imwrite(directory + "/image" + to_string(i) + ".jpg", image);
    }
    cout << "Success, pictures as been saved to " << directory << endl;
    destroyAllWindows();
    return 0;
}

int create_board()
{
    // Create Aruco dictionary and checkboard

    cout << "Creating the board" << endl;
    const Ptr<aruco::Dictionary> dictionary = getPredefinedDictionary(aruco::DICT_5X5_50);
    Mat boardImage;

    // draw board
    Ptr<aruco::CharucoBoard> charucoBoard = aruco::CharucoBoard::create(7, 7, 0.02f, 0.01f, dictionary);
    charucoBoard->draw(Size(900, 900), boardImage, 100);
    imwrite("board.png", boardImage);
    cout << "Board saved to board.png" << endl;
    return 0;
}

int picture(const string directory){

    /* Take a pictures from the camera and save it in the folder in input 
    loop until esc is presssed
    */
    cout << "Taking pictures ..." << endl
         << "Press esc to quit";

    VideoCapture cap;
    int device = 0;
    int api = CAP_ANY;

    // //! Verifier que ca fonctionne sur la RPi 1920x1080 normalement
    const vector<int> params = {CAP_PROP_FRAME_WIDTH, 1920, CAP_PROP_FRAME_HEIGHT, 1088};
    cap.open(device, api, params);

    Mat image;
    int k;
    int i = 0;
    for (int i = 0; i<=2;i++){
    if (!cap.isOpened())
    {
        cout << "Cannot open the video cam" << endl;
        return -1;
    }
    cap.read(image);
    if (image.empty())
    {
        cerr << "No image retrieved" << endl;
        return -1;
    }
   // saves the picture to the calibration_images folder
    imwrite(directory + "/image" + to_string(i) + ".jpg", image);
    }
    return 0;
}





int main()
{
    bool calibrated = false;
    String temp;
    Mat cameraMatrix, distCoeffs;
    unsigned int second = 1000000;
    string directory = "calibration_images/";
    vector<vector<Point2f>> allCharucoCorners;
    vector<vector<int>> allCharucoIds;
    int choose;
    while (choose != 6)
    {
        //clear screen
        cout << "\033[2J\033[1;1H";

        cout << "Welcome to the calibration script, current folder is :" << directory << endl
             << endl;
        if (!calibrated)
            cout << " 😥 Warning the camera as not been calibrated" << endl;
		else
			cout << "😻 The camera has been calibrated" << endl;
        cout << "0. Set a calibration folder (default = calibration_images)" << endl;
        cout << "1. Create the board to print it later" << endl;
        cout << "2. Take pictures in the calibration folder" << endl;
        cout << "3. Detect the corners" << endl;
        cout << "4. Calibrate the camera (recalculate the coefficients)" << endl;
        cout << "5. View camera coefficients" << endl;
	cout << "6. Take one photo" << endl;
        cout << "7. Quit" << endl;
        cout << endl
             << ">> N : ";
        cin >> choose;
        cout << endl;
        switch (choose)
        {
        case 0:
            cout << "Enter the folder name : ";
            cin >> directory;
			calibrated = false;
            select_directory(directory);
            break;
        case 1:
        {
            create_board();
            break;
        }
        case 2:
        {
            save_pictures(directory);
            break;
        }
        case 3:
        {
            detect_charuco(directory, true, true, allCharucoCorners, allCharucoIds);
            break;
        }
        case 4:
        {
            calibrate(directory);
			calibrated = true;
            break;
        }
        case 5:
        {
            // read the camera coefficients cameraMatrix and distCoeffs from the calibration.yml file
            FileStorage fs("calibration.yml", FileStorage::READ);
            fs["cameraMatrix"] >> cameraMatrix;
            fs["distCoeffs"] >> distCoeffs;
            fs.release();

            // print the camera matrix and distortion coefficients
            cout << "Camera Matrix : " << endl
                 << endl;
            cout << cameraMatrix << endl
                 << endl
                 << endl
                 << endl;
            cout << "Distortion Coefficients : " << endl
                 << endl;
            cout << distCoeffs << endl;
            cout << endl
                 << endl
                 << endl
                 << "Enter q to continue ...";
            cin >> temp;
            break;
        }
	case 6: 
	     cout << "Taking one photo" << endl;
	     picture(directory);
	     break;

        case 7:
            cout << "Quiting ..." << endl;
            return 0;
        default:
            cout << "Invalid choice" << endl;
            break;
        }

        usleep(second); // wait 1 seconds
        cout << endl
             << endl
             << endl;
        cout << "Returning to main menu ..." << endl;
        usleep(3 * second); // wait 1 seconds
    }

    return -1;
}
//wawww
