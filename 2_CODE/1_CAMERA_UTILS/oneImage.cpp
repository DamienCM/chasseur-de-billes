#include <opencv2/core.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>
#include <stdio.h>
using namespace cv;
using namespace std;
int main(int, char**)
{
    const bool DISPLAY_IMAGE=false;
    string temp;
    Mat frame;
    //--- INITIALIZE VIDEOCAPTURE
    VideoCapture cap;
    int device = 0;
    int api = CAP_ANY;

    // //! Verifier que ca fonctionne sur la RPi 1920x1080 normalement
    const vector<int> params = {CAP_PROP_FRAME_WIDTH, 1920, CAP_PROP_FRAME_HEIGHT, 1088};
    cap.open(device, api, params);
    // check if we succeeded
    if (!cap.isOpened()) {
        cerr << "ERROR! Unable to open camera\n";
        return -1;
    }
    //--- GRAB AND WRITE LOOP
    cout << "Start grabbing" << endl
        << "Press any key to terminate" << endl;
    for (;;)
    {
        // wait for a new frame from camera and store it into 'frame'
        cap.read(frame);
        // check if we succeeded
        if (frame.empty()) {
            cerr << "ERROR! blank frame grabbed\n";
            break;
        }
        // show live and wait for a key with timeout long enough to show images
        
        if (DISPLAY_IMAGE){
        imshow("Live", frame);
        cout<<"Press a key to take another picture or press esc to break "<<endl;
       
        int k = waitKey(0);
        if (k == 27){
            cout<<"You pressed esc"<<endl;
            break;
        } 
        }
        else{
        // save image
        imwrite("../oneImage/oneImage.jpg", frame);
        cout<<"Image saved"<<endl;
        break;
        }

    }
    // the camera will be deinitialized automatically in VideoCapture destructor
    return 0;
}